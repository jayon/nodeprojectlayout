(function() {
  var app, express, fs, stylus, program;

  fs = require('fs');
  stylus = require('stylus');
  program = require('commander');

  program
    .version('1.0')
    .usage('[options]')
    .option('-p, --port <port>', 'Puerto donde correra el servidor [8000]',
      Number, process.env.PORT || 8000)
    .option('-h, --host <IP>', 'IP donde correra el servidor [0.0.0.0]',
      String, '0.0.0.0')
    .parse(process.argv);


  function compileMethod(str) {
    return stylus(str);
  }

  express = require('express');

  app = module.exports = express.createServer();

  app.use(stylus.middleware({
    debug: true,
    src: __dirname,
    dest: __dirname,
    compile: compileMethod
  }));

  app.configure(function() {
    app.set('view', __dirname);
    app.set('view options', {
      layout: false
    });
    app.set('views engine', 'jade');
    return app.use(express.static(__dirname));
  });

  app.get('/', function(req, res) {
    return res.render(__dirname + '/index.jade');
  });

  app.listen(program.port, program.host);

  console.log(
    '  \033[90mSirviendo -> \033[36m%s\033[0m',
    program.host + ':' + program.port
  );
}).call(this);